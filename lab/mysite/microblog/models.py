from django.contrib.auth.models import User
from django.db import models


class Tags(models.Model):
    name = models.CharField(max_length=20)

    def __unicode__(self):
        return self.name


class Posts(models.Model):
    author = models.ForeignKey(User)
    title = models.CharField(max_length=200)
    text = models.TextField()
    date = models.DateTimeField()
    tags = models.ManyToManyField(Tags, blank=True, null=True)



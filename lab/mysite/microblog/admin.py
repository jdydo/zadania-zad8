from django.contrib import admin
from models import Posts, Tags


class PostsAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'date')
    list_filter = ('date', 'tags')
    search_fields = ['tags__name']
    ordering = ('-date', )


class TagsAdmin(admin.ModelAdmin):
    list_display = ('name', )


admin.site.register(Posts, PostsAdmin)
admin.site.register(Tags, TagsAdmin)
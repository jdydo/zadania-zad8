from django.contrib.auth.models import User
from models import Posts, Tags
from django.utils import timezone
from sample_content import *


def delete_all():
    Posts.objects.all().delete()
    User.objects.all().delete()
    Tags.objects.all().delete()


def add_sample_content():
    user1 = User.objects.create_user(
        username='Shepard', password='Shepard', email='shepard@citadel.com', first_name='Commander', last_name='Shepard'
    )
    user2 = User.objects.create_user(
        username='Miranda', password='Miranda', email='miranda@citadel.com', first_name='Miranda', last_name='Lawson'
    )
    user3 = User.objects.create_user(
        username='Urdnot', password='Urdnot', email='urdnot@citadel.com', first_name='Urdnot', last_name='Wrex'
    )

    user1.save()
    user2.save()
    user3.save()

    tag1 = Tags(name='Citadel')
    tag2 = Tags(name='Urdnot')
    tag3 = Tags(name='Shepard')
    tag4 = Tags(name='Miranda')
    tag5 = Tags(name='Star Fleet')
    tag6 = Tags(name='Battle of Human Kind')
    tag7 = Tags(name='Sovereign')
    tag8 = Tags(name='Destroyers')

    tag1.save()
    tag2.save()
    tag3.save()
    tag4.save()
    tag5.save()
    tag6.save()
    tag7.save()
    tag8.save()

    date1 = timezone.now().replace(2014, 4, 21)
    date2 = timezone.now().replace(2014, 4, 20)
    date3 = timezone.now().replace(2014, 4, 19)
    date4 = timezone.now().replace(2014, 4, 12)
    date5 = timezone.now().replace(2014, 4, 1)
    date6 = timezone.now().replace(2014, 3, 15)
    date7 = timezone.now().replace(2013, 1, 1)

    post1 = Posts(author=user1, text=CONTENT_1, title='Episode I', date=date7)
    post2 = Posts(author=user1, text=CONTENT_2, title='Episode II', date=date6)
    post3 = Posts(author=user1, text=CONTENT_3, title='Episode III', date=date5)
    post4 = Posts(author=user2, text=CONTENT_4, title='Episode IV', date=date4)
    post5 = Posts(author=user2, text=CONTENT_5, title='Episode V', date=date3)
    post6 = Posts(author=user3, text=CONTENT_6, title='Episode VI', date=date2)
    post7 = Posts(author=user3, text=CONTENT_7, title='Episode VII', date=date1)

    post1.save()
    post2.save()
    post3.save()
    post4.save()
    post5.save()
    post6.save()
    post7.save()

    post1.tags.add(tag1)
    post1.tags.add(tag3)
    post1.tags.add(tag4)
    post2.tags.add(tag1)
    post2.tags.add(tag3)
    post4.tags.add(tag6)
    post4.tags.add(tag7)
    post4.tags.add(tag8)
    post4.tags.add(tag1)
    post5.tags.add(tag3)
    post5.tags.add(tag1)
    post6.tags.add(tag4)
    post7.tags.add(tag5)




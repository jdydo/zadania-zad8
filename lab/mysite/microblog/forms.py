from django.forms import ModelForm, TextInput, HiddenInput
from django import forms
from models import Posts


class PostForm(ModelForm):
    author_name = forms.CharField(widget=TextInput(attrs={'readonly': 'readonly'}))

    class Meta:
        model = Posts
        fields = [
            'author', 'author_name', 'title', 'text', 'tags', 'date'
        ]
        widgets = {
            'author': HiddenInput(),
            'date': HiddenInput(),
        }

    def clean_title(self):
        data = self.cleaned_data['title']
        if len(data) < 5 or len(data) > 50:
            raise forms.ValidationError("Tytul musi miec minimum 5, a maksimum 200 znakow!")
        return data

    def clean_text(self):
        data = self.cleaned_data['text']
        if len(data) < 10 or len(data) > 200:
            raise forms.ValidationError("Wpis musi miec minimum 10, a maksimum 200 znakow!")
        return data
from django.views.generic import View
from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone
from models import Posts
from forms import PostForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from setup_sample_content import delete_all, add_sample_content


class AllPosts(View):
    def get(self, request):
        return render(request, 'microblog/show_posts.html', {'posts': Posts.objects.all().order_by('-id')})


class ChooseUser(View):
    def get(self, request):
        return render(request, 'microblog/choose_user.html', {'users': User.objects.all().order_by('username')})


class UserPosts(View):
    def get(self, request, user_id):
        return render(request, 'microblog/show_posts.html',
                      {'posts': get_object_or_404(User, pk=user_id).posts_set.all().order_by('-id')})


class AddPost(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'microblog/add_post.html', {
            'form': PostForm(initial={
                'author': request.user,
                'author_name': request.user.first_name + ' ' + request.user.last_name,
                'date': timezone.now()
            })
        })

    def post(self, request, *args, **kwargs):
        form = PostForm(request.POST)
        if form.is_valid():
            form.save()
            return render(request, 'microblog/ok.html', {'text': 'Post has been added!'})
        else:
            return render(request, 'microblog/add_post.html', {'form': form})


class Login(View):
    def get(self, request):
        return render(request, 'microblog/login.html')

    def post(self, request):
        user = authenticate(username=request.POST['username'], password=request.POST['password'])
        if user is not None:
            login(request, user)
            return render(request, 'microblog/ok.html', {'text': 'Logged in!'})
        else:
            return render(request, 'microblog/login.html', {'error': 'Error! Wrong username or password.'})


class Logout(View):
    def get(self, request, *args, **kwargs):
        logout(request)
        return render(request, 'microblog/ok.html', {'text': 'Logged out!'})


class Reset(View):
    def get(self, request):
        delete_all()
        add_sample_content()
        return redirect('all_posts')



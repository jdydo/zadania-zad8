from django.forms import ModelForm, HiddenInput, PasswordInput, EmailField
from django import forms
from models import Posts
from django.contrib.auth.models import User


class PostForm(ModelForm):

    class Meta:
        model = Posts
        fields = [
            'author', 'title', 'text', 'tags', 'date'
        ]
        widgets = {
            'author': HiddenInput(),
            'date': HiddenInput(),
        }

    def clean_title(self):
        data = self.cleaned_data['title']
        if len(data) < 5 or len(data) > 50:
            raise forms.ValidationError("Title length should be between 5 and 50 characters.")
        return data

    def clean_text(self):
        data = self.cleaned_data['text']
        if len(data) < 10 or len(data) > 1000:
            raise forms.ValidationError("Text length should be between 10 and 1000 characters.")
        return data

    def clean_tags(self):
        data = self.cleaned_data['tags']
        if len(data) > 5:
            raise forms.ValidationError("You can choose maximum 5 tags.")
        return data


class UserForm(ModelForm):

    class Meta:
        model = User
        fields = [
            'username', 'password', 'email'
        ]
        widgets = {
            'password': PasswordInput(),
        }

    def clean_username(self):
        data = self.cleaned_data['username']
        if len(data) < 5 or len(data) > 15:
            raise forms.ValidationError("Username length should be between 5 and 15 characters.")
        elif User.objects.all().filter(username=data):
            raise forms.ValidationError("Username already exists.")
        return data

    def clean_password(self):
        data = self.cleaned_data['password']
        if len(data) < 5 or len(data) > 15:
            raise forms.ValidationError("Password length should be between 5 and 15 characters.")
        return data

    def clean_email(self):
        data = self.cleaned_data['email']
        if not data:
            raise forms.ValidationError("This field is required.")
        elif User.objects.all().filter(email=data):
            raise forms.ValidationError("Email already registered.")
        return data
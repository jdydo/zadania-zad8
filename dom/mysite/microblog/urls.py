from django.contrib.auth.decorators import login_required
from views import *
from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', AllPosts.as_view(), name='all_posts'),

    url(r'^user/$', ChooseUser.as_view(), name='choose_user'),
    url(r'^user/(?P<user_id>\d+)/$', UserPosts.as_view(), name='user_posts'),

    url(r'^tag/$', ChooseTag.as_view(), name='choose_tag'),
    url(r'^tag/(?P<tag_id>\d+)/$', PostTags.as_view(), name='post_tags'),

    url(r'^add/$', login_required(AddPost.as_view()), name='add_post'),
    url(r'^edit/(?P<post_id>\d+)/$', login_required((EditPost.as_view())), name='edit_post'),

    url(r'^register/$', Register.as_view(), name='register'),
    url(r'^register/accept/(?P<user_id>\d+)/$', Accept.as_view(), name='accept'),

    url(r'^login/$', Login.as_view(), name='login'),
    url(r'^logout/$', login_required(Logout.as_view()), name='logout'),

    url(r'^admin/', include(admin.site.urls)),

    url(r'^reset/$', Reset.as_view()),
)
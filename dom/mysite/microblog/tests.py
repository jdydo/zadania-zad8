# -*- coding=utf-8 -*-

from django.test import TestCase
from django.test.client import Client
from django.test.utils import setup_test_environment
from models import Posts, Tags
from django.contrib.auth.models import Group, Permission, User
from django.contrib.contenttypes.models import ContentType
from django.utils import timezone
from django.core import mail
from datetime import timedelta



setup_test_environment()
client = Client()


class MicroblogTest(TestCase):
    def extend_html_check(self, response, logged):
        if logged:
            assert 'All Posts' in response.content
            assert 'User Posts' in response.content
            assert 'Tags' in response.content
            assert 'Add Post' in response.content
            assert 'Log out' in response.content
            assert 'Log in' not in response.content
        else:
            assert 'All Posts' in response.content
            assert 'User Posts' in response.content
            assert 'Tags' in response.content
            assert 'Add Post' not in response.content
            assert 'Log out' not in response.content
            assert 'Log in' in response.content

    def save(self):
        self.tag1.save()
        self.tag2.save()
        self.post1.save()
        self.post2.save()

    def setUp(self):
        self.edit_perm = Permission.objects.create(
            codename='can_edit',
            name='Can Edit Posts',
            content_type=ContentType.objects.get_for_model(Posts)
        )
        self.edit_perm.save()

        self.group = Group(name='Moderators')
        self.group.save()
        self.group.permissions.add(self.edit_perm)

        user0 = User.objects.create_user(
            username='Garrus', password='Garrus', email='admind@citadel.com', first_name='Admin', last_name='Admin'
        )
        user0.is_staff = True
        user0.is_superuser = True
        user0.save()

        self.user1 = User.objects.create_user(
            username='Shepard', password='Shepard', email='shepard@citadel.com', first_name='Commander',
            last_name='Shepard'
        )
        self.user2 = User.objects.create_user(
            username='Miranda', password='Miranda', email='miranda@citadel.com', first_name='Miranda',
            last_name='Lawson'
        )

        self.user1.groups.add(self.group)

        self.tag1 = Tags(name='Citadel')
        self.tag2 = Tags(name='Star Fleet')

        self.post1 = Posts(
            author=self.user1, title='Some interesting Title1', text='Some interesting Text1',
            date=timezone.now()
        )
        self.post2 = Posts(
            author=self.user2, title='Some interesting Title2', text='Some interesting Text2',
            date=timezone.now()
        )

    def add_many_to_many(self):
        self.post1.tags.add(self.tag1)
        self.post2.tags.add(self.tag2)

    def test_empty_post_base(self):
        assert Posts.objects.count() == 0

    def test_empty_tag_base(self):
        assert Tags.objects.count() == 0

    def test_filled_user_base(self):
        self.user1.save()
        assert User.objects.count() == 3

    def test_filled_post_base(self):
        self.save()
        assert Posts.objects.count() == 2

    def test_filled_tags_base(self):
        self.save()
        assert Tags.objects.count() == 2

    def test_model(self):
        self.save()
        post = Posts.objects.filter(id=1)[0]
        assert 'Shepard' == post.author.username
        assert 'Some interesting Title1' == post.title
        assert 'Some interesting Text1' == post.text

    def test_404a(self):
        assert client.get('/addaaa').status_code == 404

    def test_get_empty_blog(self):
        response = client.get('/')
        assert response.status_code == 200
        assert len(response.context['posts']) == 0
        self.extend_html_check(response, False)

    def test_get_filled_blog(self):
        self.save()
        response = client.get('/')
        assert response.status_code == 200
        assert len(response.context['posts']) == 2
        self.extend_html_check(response, False)

    def test_tags_display(self):
        self.save()
        self.add_many_to_many()
        response = client.get('/')
        assert 'Citadel' in response.content
        assert 'Star Fleet' in response.content

    def test_get_user_list(self):
        self.save()
        response = client.get('/user/')
        assert response.status_code == 200
        assert len(response.context['users']) == 3
        assert 'Miranda' in response.content
        assert 'Shepard' in response.content
        self.extend_html_check(response, False)

    def test_get_user_posts(self):
        self.save()
        response = client.get('/user/2/')
        assert response.status_code == 200
        assert 'Shepard' in response.content
        assert 'Some interesting Title1' in response.content
        assert 'Some interesting Text1' in response.content
        assert 'Miranda Lawson' not in response.content
        assert 'Some interesting Title2' not in response.content
        assert 'Some interesting Text2' not in response.content
        self.extend_html_check(response, False)

    def test_get_tags_list(self):
        self.save()
        self.add_many_to_many()
        response = client.get('/tag/')
        assert response.status_code == 200
        assert len(response.context['tags']) == 2
        assert 'Citadel' in response.content
        assert 'Star Fleet' in response.content
        self.extend_html_check(response, False)

    def test_get_tags_posts(self):
        self.save()
        self.add_many_to_many()
        response = client.get('/tag/1/')
        assert response.status_code == 200
        assert 'Shepard' in response.content
        assert 'Some interesting Title1' in response.content
        assert 'Some interesting Text1' in response.content
        assert 'Miranda Lawson' not in response.content
        assert 'Some interesting Title2' not in response.content
        assert 'Some interesting Text2' not in response.content
        self.extend_html_check(response, False)

    def test_empty_user_context(self):
        assert len(client.get('/user/7/').context['posts']) == 0

    def test_empty_tag_context(self):
        assert len(client.get('/tag/7/').context['posts']) == 0

    def test_get_login(self):
        response = client.get('/login/')
        assert response.status_code == 200
        self.extend_html_check(response, False)
        assert 'name="username"' in response.content
        assert 'name="password"' in response.content

    def test_post_login_with_valid_data(self):
        self.save()
        response = client.post('/login/', {'username': 'Shepard', 'password': 'Shepard'}, follow=True)
        assert response.status_code == 200
        assert 'Hello, Shepard!' in response.content
        self.extend_html_check(response, True)
        assert 'Logged in!' in response.content

    def test_post_login_with_invalid_data(self):
        self.save()
        response = client.post('/login/', {'username': 'bad', 'password': 'bad'}, follow=True)
        assert response.status_code == 200
        assert 'Wrong username or password.' in response.content
        self.extend_html_check(response, False)
        assert 'name="username"' in response.content
        assert 'name="password"' in response.content

    def test_get_add_without_login(self):
        response = client.get('/add/', follow=True)
        assert response.status_code == 200
        self.extend_html_check(response, False)
        assert 'name="username"' in response.content
        assert 'name="password"' in response.content

    def test_get_add_with_login(self):
        self.save()
        client.post('/login/', {'username': 'Shepard', 'password': 'Shepard'})
        response = client.get('/add/')
        assert response.status_code == 200
        assert 'Hello, Shepard!' in response.content
        self.extend_html_check(response, True)
        assert 'name="title"' in response.content
        assert 'name="text"' in response.content
        assert 'name="date" type="hidden"' in response.content

    def test_post_add_without_login(self):
        response = client.post('/add/', {'title': 'Some Title', 'text': 'Some Text'}, follow=True)
        assert response.status_code == 200
        self.extend_html_check(response, False)
        assert 'name="username"' in response.content
        assert 'name="password"' in response.content

    def test_post_add_with_login_and_valid_data(self):
        self.save()
        client.login(username='Shepard', password='Shepard')
        response = client.post('/add/', {'title': 'Some interesting Title3', 'text': 'Some interesting Text3'})
        assert response.status_code == 200
        assert 'Post has been added!' in response.content
        response = client.get('/')
        assert len(response.context['posts']) == 3
        assert 'Hello, Shepard!' in response.content
        self.extend_html_check(response, True)
        assert 'Some interesting Text3' in response.content
        assert 'Some interesting Title3' in response.content

    def test_post_add_with_login_and_empty_data(self):
        self.save()
        client.post('/login/', {'username': 'Shepard', 'password': 'Shepard'})
        response = client.post('/add/', {'title': '', 'text': ''}, follow=True)
        assert response.status_code == 200
        assert 'This field is required.' in response.content
        assert 'Hello, Shepard!' in response.content
        self.extend_html_check(response, True)
        assert 'name="title"' in response.content
        assert 'name="text"' in response.content

    def test_post_add_with_login_and_invalid_data(self):
        self.save()
        client.post('/login/', {'username': 'Shepard', 'password': 'Shepard'})
        response = client.post('/add/', {'title': 'aa', 'text': 'bb'}, follow=True)
        assert response.status_code == 200
        assert 'Title length should be between 5 and 50 characters.' in response.content
        assert 'Text length should be between 10 and 1000 characters.' in response.content
        assert 'Hello, Shepard!' in response.content
        self.extend_html_check(response, True)
        assert 'name="title"' in response.content
        assert 'name="text"' in response.content

    def test_get_logout(self):
        self.save()
        client.post('/login/', {'username': 'Shepard', 'password': 'Shepard'})
        response = client.get('/logout/', follow=True)
        assert response.status_code == 200
        assert 'Hello, Shepard!' not in response.content
        self.extend_html_check(response, False)
        assert 'Logged out!' in response.content

    def test_get_register_invalid2(self):
        response = client.get('/register/')
        assert response.status_code == 200
        self.extend_html_check(response, False)
        assert 'name="username"' in response.content
        assert 'name="password"' in response.content
        assert 'name="email"' in response.content

    def test_post_register_valid(self):
        response = client.post('/register/', {'username': 'test1', 'password': 'test2', 'email': 'test@test.com'}, follow=True)
        assert response.status_code == 200
        self.extend_html_check(response, False)
        assert 'Success! Check your mail!' in response.content
        assert 'http://194.29.175.240/~p1/wsgi/register/accept/4' in mail.outbox[0].body

        response = client.post('/login/', {'username': 'test1', 'password': 'test2'}, follow=True)
        assert response.status_code == 200
        assert 'User is inactive.' in response.content

        response = client.get('/register/accept/4/')
        assert 'Success! Email confirmed!' in response.content

        response = client.post('/login/', {'username': 'test1', 'password': 'test2'}, follow=True)
        assert response.status_code == 200
        assert 'Logged in!' in response.content

    def test_post_register_invalid1(self):
        response = client.post('/register/', {'username': 't1', 'password': 't2', 'email': 'test@test.com'}, follow=True)
        assert response.status_code == 200
        self.extend_html_check(response, False)
        assert 'Username length should be between 5 and 15 characters.' in response.content
        assert 'Password length should be between 5 and 15 characters.' in response.content

    def test_post_register_invalid2(self):
        response = client.post('/register/', {'username': 'Shepard', 'password': 'test2', 'email': 'shepard@citadel.com'}, follow=True)
        assert response.status_code == 200
        self.extend_html_check(response, False)
        assert 'Username already exists.' in response.content
        assert 'Email already registered.' in response.content

    def test_get_edit_without_login(self):
        self.post1.date = self.post1.date - timedelta(minutes=10)
        self.save()
        response = client.get('/edit/1/', follow=True)
        assert 'name="username"' in response.content
        assert 'name="password"' in response.content

    def test_get_edit_with_login_within_time(self):
        self.save()
        client.login(username='Miranda', password='Miranda')
        response = client.get('/edit/2/')
        assert response.status_code == 200
        assert 'name="title"' in response.content
        assert 'name="text"' in response.content
        assert 'name="date" type="hidden"' in response.content
        assert 'Some interesting Text2' in response.content
        assert 'Some interesting Title2' in response.content

    def test_get_edit_with_login_within_time_other_post(self):
        self.save()
        client.login(username='Miranda', password='Miranda')
        response = client.get('/edit/1/', follow=True)
        assert response.status_code == 200
        assert 'Access Denied!' in response.content

    def test_get_edit_with_login_without_time(self):
        self.post2.date = self.post2.date - timedelta(minutes=10)
        self.save()
        client.login(username='Miranda', password='Miranda')
        response = client.get('/edit/2/', follow=True)
        assert response.status_code == 200
        assert 'Access Denied!' in response.content

    def test_get_edit_with_login_without_time_with_moderator_acc(self):
        self.post2.date = self.post2.date - timedelta(minutes=10)
        self.save()
        client.login(username='Shepard', password='Shepard')
        response = client.get('/edit/2/', follow=True)
        assert response.status_code == 200
        assert 'name="title"' in response.content
        assert 'name="text"' in response.content
        assert 'Some interesting Text2' in response.content
        assert 'Some interesting Title2' in response.content

    def test_post_edit_with_login_and_edited_display(self):
        self.save()
        client.login(username='Garrus', password='Garrus')
        response = client.post('/edit/2/', {'title': 'Some interesting Title3', 'text': 'Some interesting Text3'})
        assert response.status_code == 200
        assert 'Post has been edited!' in response.content
        response = client.get('/')
        assert len(response.context['posts']) == 2
        assert 'Hello, Garrus!' in response.content
        self.extend_html_check(response, True)
        assert 'Some interesting Text3' in response.content
        assert 'Some interesting Title3' in response.content
        assert 'Last update:' in response.content
        assert 'Garrus' in response.content

    def test_post_edit_without_login(self):
        self.save()
        response = client.post('/edit/2/', {'title': 'Some interesting Title3', 'text': 'Some interesting Text3'}, follow=True)
        assert response.status_code == 200
        self.extend_html_check(response, False)
        assert 'name="username"' in response.content
        assert 'name="password"' in response.content

    def test_admin_link(self):
        self.save()
        client.login(username='Garrus', password='Garrus')
        response = client.get('/')
        assert 'Admin Panel' in response.content

    def test_admin_filters(self):
        self.save()
        client.login(username='Garrus', password='Garrus')
        response = client.get('/admin/microblog/posts/')
        assert 'Filter' in response.content
        assert 'By date' in response.content
        assert 'By tags' in response.content
        assert 'searchbar' in response.content

    def test_get_edit_with_login_and_edited_display(self):
        self.save()
        self.add_many_to_many()
        client.login(username='Garrus', password='Garrus')
        response = client.get('/admin/microblog/posts/', {'q': 'Citadel'})
        assert response.status_code == 200
        assert 'Some interesting Title1' in response.content
        assert 'Shepard' in response.content
        assert 'Some interesting Title2' not in response.content
        assert 'Miranda' not in response.content



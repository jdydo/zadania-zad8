from django.views.generic import View
from django.shortcuts import render, redirect, get_object_or_404
from models import Posts, Tags
from forms import PostForm, UserForm
from django.contrib.auth.models import User
from django.contrib.auth import login, logout
from setup_sample_content import delete_all, add_sample_content, add_primary_content
from methods import *
from django.contrib.auth.forms import AuthenticationForm
from django.core.mail import send_mail


class AllPosts(View):
    def get(self, request):
        """
        Wypisywanie wszystkich wpisow z bazy
        """
        return render(request, 'microblog/show_posts.html', {'posts': Posts.objects.all().order_by('-id')})


class ChooseUser(View):
    def get(self, request):
        """
        Wypisywanie uzytkownikow z bazy
        """
        return render(request, 'microblog/choose.html', {'users': User.objects.all().order_by('username')})


class ChooseTag(View):
    def get(self, request):
        """
        Wypisywanie tagow z bazy
        """
        return render(request, 'microblog/choose.html', {'tags': Tags.objects.all().order_by('name')})


class UserPosts(View):
    def get(self, request, user_id):
        """
        Wypisywanie wpisow danego uzytkownika
        """
        return render(request, 'microblog/show_posts.html',
                      {'posts': Posts.objects.all().filter(author=User.objects.all().filter(id=user_id)).order_by('-id')})


class PostTags(View):
    def get(self, request, tag_id):
        """
        Wypisywanie wpisow z danym tagiem
        """
        return render(request, 'microblog/show_posts.html',
                      {'posts': Posts.objects.all().filter(tags__id=tag_id).order_by('-id')})


class AddPost(View):
    def get(self, request, *args, **kwargs):
        """
        Wyswietlanie formularza dodawania wpisow
        """
        return render(request, 'microblog/add_post.html', {
            'form': PostForm(initial={
                'author': request.user,
                'author_name': request.user.username,
                'date': timezone.now()
            })
        })

    def post(self, request, *args, **kwargs):
        """
        Walidowanie i dodawanie wpisu do bazy
        """
        form = PostForm(request.POST)
        if form.is_valid():
            form.save()
            return render(request, 'microblog/ok.html', {'text': 'Post has been added!'})
        else:
            return render(request, 'microblog/add_post.html', {'form': form})


class EditPost(View):
    def get(self, request, post_id):
        """
        Wyswietlanie formularza edycji wpisu
        """
        post = get_object_or_404(Posts, id=post_id)
        if is_editable(post, request.user):
            return render(request, 'microblog/edit_post.html', {'form': PostForm(instance=post), 'id': post.pk})
        else:
            return render(request, 'microblog/ok.html', {'text': 'Access Denied!'})

    def post(self, request, post_id):
        """
        Walidacja i zapisywanie efektow edycji wpisu
        """
        post = get_object_or_404(Posts, id=post_id)
        post.last_edited_by = request.user
        post.last_edited_date = timezone.now()
        form = PostForm(request.POST, instance=post)
        if is_editable(post, request.user):
            if form.is_valid():
                form.save()
                return render(request, 'microblog/ok.html', {'text': 'Post has been edited!'})
            else:
                return render(request, 'microblog/edit_post.html', {'form': form, 'id': post_id})
        else:
            return render(request, 'microblog/ok.html', {'text': 'Access Denied!'})


class Login(View):
    def get(self, request):
        """
        Wyswietlanie formularza logowania
        """
        return render(request, 'microblog/login.html', {'form': AuthenticationForm()})

    def post(self, request):
        """
        Walidacja i logowanie uzytkownika
        """
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            login(request, form.get_user())
            return render(request, 'microblog/ok.html', {'text': 'Logged in!'})
        elif form.get_user() and not form.get_user().is_active:
            return render(request, 'microblog/login.html', {'error': 'User is inactive.', 'form': AuthenticationForm()})
        else:
            return render(request, 'microblog/login.html', {'error': 'Wrong username or password.',
                                                            'form': AuthenticationForm()})


class Register(View):
    def get(self, request):
        """
        Wyswietlanie formularza rejestracji
        """
        return render(request, "microblog/register.html", {'form': UserForm})

    def post(self, request):
        """
        Walidacja, rejestracja uzytkownika, wysylanie maila aktywacyjnego
        """
        form = UserForm(request.POST)
        if form.is_valid():
            user = User.objects.create_user(
                username=form.cleaned_data['username'],
                password=form.cleaned_data['password'],
                email=form.cleaned_data['email']
            )
            user.is_active = False
            user.save()

            send_mail(
                'PWI Microblog - Account activation',
                'Hi, %s!\r\nHere is your activation link:\r\nhttp://194.29.175.240/~p1/wsgi/register/accept/%s'
                % (form.cleaned_data['username'],
                   get_object_or_404(User, username=form.cleaned_data['username']).id),
                'pwi.microblog@gmail.com',
                [form.cleaned_data['email']],
                fail_silently=True
            )
            return render(request, 'microblog/ok.html', {'text': 'Success! Check your mail!'})
        else:
            return render(request, "microblog/register.html", {'form': form})


class Logout(View):
    def get(self, request, *args, **kwargs):
        """
        Wylogowanie uzytkownika
        """
        logout(request)
        return render(request, 'microblog/ok.html', {'text': 'Logged out!'})


class Accept(View):
    def get(self, request, user_id):
        """
        Aktywacja uzytkownika
        """
        user = get_object_or_404(User, id=user_id)
        if user.is_active:
            return render(request, 'microblog/ok.html', {'text': 'Email already confirmed!'})
        else:
            user.is_active = True
            user.save()
            return render(request, 'microblog/ok.html', {'text': 'Success! Email confirmed!'})


class Reset(View):
    def get(self, request):
        try:
            delete_all()
            #add_primary_content()
            add_sample_content()
        except:
            pass
        return redirect('all_posts')
from django.contrib.auth.models import User
from django.db import models


class Tags(models.Model):
    name = models.CharField(max_length=20)

    def __unicode__(self):
        return self.name


class Posts(models.Model):
    author = models.ForeignKey(User, blank=True, null=True, related_name='Author')
    title = models.CharField(max_length=200)
    text = models.TextField()
    date = models.DateTimeField(blank=True, null=True)
    tags = models.ManyToManyField(Tags, blank=True, null=True)
    last_edited_date = models.DateTimeField(blank=True, null=True)
    last_edited_by = models.ForeignKey(User, blank=True, null=True,  related_name='Last edited by')